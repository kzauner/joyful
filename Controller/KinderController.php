<?php

namespace Controller;

use \Template;

class KinderController
{
    /**
     * Liefert die View um die Kinderliste anzuzeigen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function index($f3, $params)
    {

        $km = new \Models\KinderModel();
        $kinder = $km->kinder();
        $f3->set('kind', $kinder);
        $kom = new \Models\KontaktModel();
        $kontakt = $kom->kontaktShow($kinder[0]['kind_id']);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kinderliste');
        $f3->set('mainHeading', 'Kinder');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kinder.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Listenansicht der Kinder
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kinder($f3, $params)
    {
        $db = new \DB\SQL('mysql:host=localhost;port=3306;dbname=joyful', 'joyfuluser', 'asdfjiwerijsdw3r');

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kinder');
        $f3->set('mainHeading', 'Kinder');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kinder.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        $f3->set('resultKind', $db->exec('SELECT * FROM kind'));
        $f3->set('resultKontakt', $db->exec('SELECT * FROM kontaktperson'));
        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Gruppenansicht der Kinder
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kinderGroup($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kinder');
        $f3->set('mainHeading', 'Kinder');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kinder-groupView.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Ein einzelnes Kind anzeigen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kindShow($f3, $params)
    {
        $kid = $params['kid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($kid, FILTER_VALIDATE_INT)) {
            $kinder = [];
        } else {
            $km = new \Models\KinderModel();
            $kinder = $km->kind($kid);
            $am = new \Models\AvatarModel();
            $avatar = $am->avatarShow($kinder[0]['kind_id']);
        }

        $f3->set('kind', $kinder);
        $f3->set('avatar', $avatar);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kind anzeigen');
        $f3->set('mainHeading', 'Kind anzeigen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kind-show.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Datensätze eines Kindes ändern
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kindEdit($f3, $params)
    {
        $kid = $params['kid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($kid, FILTER_VALIDATE_INT)) {
            $kinder = [];
        } else {
            $km = new \Models\KinderModel();
            $kinder = $km->kind($kid);
            $am = new \Models\AvatarModel();
            $avatar = $am->avatarShow($kinder[0]['kind_id']);
        }
        $f3->set('kind', $kinder);
        $f3->set('avatar', $avatar);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kind anzeigen');
        $f3->set('mainHeading', 'Kind anzeigen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kind-edit.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Überschreibt die vorhanden Datensätze eines Kindes
     * wird für POST verwendet
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kindUpdate($f3, $params)
    {
        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'vorname'   => 'required|alpha_space,255',
                'nachname'  => 'required|alpha_space,255',
                'versNr'   => 'required|numeric|exact_len,4',
                'gebTag'   => 'required',
                'kundNr'   => 'required',
                'adresse'   => 'required|max_len,255',
                'plz'   => 'required|numeric|min_len,4',
                'ort'   => 'required|alpha_space,255',
                'muttersprache' => 'required|alpha_space,255',
                'geschwister'   =>  'max_len,255',
                'start' => 'required',
                'gruppe'    => 'required',
                'fruehbetreuung'    => 'numeric',
                'halbtags'  => 'numeric',
                'ganztags'  => 'numeric',
                'allergien' => 'alpha_space,255',
                'krankheiten'   => 'alpha_space,255',
                'windeln'   => 'numeric',
                'schnuller' => 'numeric'
            ));
            $validData = $gump->run($_POST);
        }
      //  var_dump($params['kid']); exit();
        if (!filter_var($params['kid'], FILTER_VALIDATE_INT)) {
            $kinder = [];
        } else {
            $km = new \Models\KinderModel();
            $f3->set('kind_id', $params['kid']);
            $kinder = $km->kindEdit($validData, $params['kid']);
            $f3->reroute('/kinder');
        }

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kind anzeigen');
        $f3->set('mainHeading', 'Kind anzeigen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kind-edit.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');
    
        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Kind aus der DB löaschen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kindDelete($f3, $params) {
        $uid = $params['kid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($uid, FILTER_VALIDATE_INT)) {
            echo 'Error: Kind konnte nicht gelöscht werden';
        } else {
            $um = new \Models\KinderModel();
            if ($um->kindDelete($uid)) {
                echo '1'; // Bei Erfolg wird 1 zurück gegeben
                $f3->reroute('/kinder');
            }
            else {
                echo 'Error';
            }
        }
    }
    /**
     * Zeigt die Kontaktperson zum jeweiligen Kind an
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kontaktShow($f3, $params) 
    {
        $kid = $params['kid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($kid, FILTER_VALIDATE_INT)) {
            $kontaktperson = [];
        } else {
            $km = new \Models\KontaktModel();
            $kontaktperson = $km->kontaktShow($kid);
        }

        $f3->set('kontaktperson', $kontaktperson);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Kind anzeigen');
        $f3->set('mainHeading', 'Kind anzeigen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kontakt-show.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }
}
