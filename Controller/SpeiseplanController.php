<?php

namespace Controller;

use \Template;

class SpeiseplanController
{
    /**
     * Liefert die view für den Speiseplan
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function speiseplan($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Speiseplan');
        $f3->set('mainHeading', 'Speiseplan');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/speiseplan.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }
    /**
     * Liefert die View um einen neuen Speiseplan zu speichern
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function speiseplanNeu($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Neuer Speiseplan');
        $f3->set('mainHeading', 'Neuer Speiseplan');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/speiseplan-neu.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }
}
