<?php

namespace Controller;

use \Template;

class KontaktController
{
    /**
     * Neue Kontaktperson anlegen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function neuerKontakt($f3, $params)
    {
        $db = new \DB\SQL('mysql:host=localhost;port=3306;dbname=joyful', 'joyfuluser', 'asdfjiwerijsdw3r');
        
        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Neues Kind anlegen');
        $f3->set('mainHeading', 'Neues Kind anlegen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/neuer-kontakt.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        $f3->set('result', $db->exec('SELECT * FROM kind'));
        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Validierung des Kontaktpersonen-Formulares und ruft die Methode
     * beim KontaktModel auf, um die validierten Daten in die Datenbank 
     * zu speichern.
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kontaktSpeichern($f3, $params)
    {

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'kind_id'   => 'required',
                'bevorzugt' => 'numeric',
                'beziehung' => 'required|alpha_space,255',
                'famstand' => 'required|alpha_space,255',
                'vorname'   => 'required|alpha_space,255',
                'nachname'  => 'required|alpha_space,255',
                'adresse'   => 'required|max_len,255',
                'plz'   => 'required',
                'ort'   => 'required',
                'telefon'   => 'required',
                'email' => 'required'

            ));
            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            } else {
                $km = new \Models\KontaktModel();
                $isStored = $km->kontaktpersonSpeichernDB($validData);

                if ($isStored) {
                    $f3->set('alertSuccess', 'Kontaktperson erfolgreich gespeichert!');
                    $f3->reroute('/kinder?success=ja');
                } else {
                    $f3->set('alertError', 'Fehler! Daten der Kontaktperson konnten nicht gespeichert werden.');
                }
            }
        }
    }
}
