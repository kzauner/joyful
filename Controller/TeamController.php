<?php

namespace Controller;

use \Template;

class TeamController
{
    /**
     * Liefert die View um das Team anzuzeigen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function index($f3, $params)
    {

        $tm = new \Models\TeamModel();
        $team = $tm->team();
        $f3->set('team', $team);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Team');
        $f3->set('mainHeading', 'Team');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/team.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Listenansicht der Teammitglieder
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function team($f3, $params)
    {

        $db = new \DB\SQL('mysql:host=localhost;port=3306;dbname=joyful', 'joyfuluser', 'asdfjiwerijsdw3r');

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Team');
        $f3->set('mainHeading', 'Team');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/team.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');
        $f3->set('result', $db->exec('SELECT * FROM team'));
        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Neues Teammitglied hinzufügen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function teamNew($f3, $params)
    {

        // Formular wurde geschickt
        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'nachname'  => 'required|alpha_space,255',
                'vorname'   => 'required|alpha_space,255',
                'versNr'    => 'required',
                'gebTag'    => 'required',
                'adresse'   => 'required|max_len,255',
                'plz'   => 'required',
                'ort'   => 'required',
                'telefon'   => 'required',
                'email' => 'required',
                'dienstvertrag' => 'required',
                'gruppe'    => 'required',
                'funktion'  => 'required'
            ));
            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            } else {
                $km = new \Models\TeamModel();
                $isStored = $km->teamSave($validData);

                if ($isStored > 0) {
                    $f3->set('alertSuccess', 'Teammitglied erfolgreich gespeichert!');
                    $f3->set('UPLOADS', 'uploads/');
                    $overwrite = false;
                    $slug = true;
                    $web = \Web::instance();
                    $files = $web->receive(
                        function ($file, $avatar) {
                            return true;
                        },
                        $overwrite,
                        $slug
                    );
                } else {
                    $f3->set('alertError', 'Fehler! Daten konnten nicht gespeichert werden.');
                }
            }
        }

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Neues Teammitglied anlegen');
        $f3->set('mainHeading', 'Neues Teammitglied anlegen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/team-new.html');
        $f3->set('lastID', $isStored);
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Ein einzelnes Teammitglied anzeigen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function teamShow($f3, $params)
    {
        $tid = $params['tid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($tid, FILTER_VALIDATE_INT)) {
            $team = [];
        } else {
            $tm = new \Models\TeamModel();
            $team = $tm->teamShow($tid);
            $am = new \Models\AvatarModel();
            $avatar = $am->avatarShow($team[0]['team_id']);
        }

        $f3->set('team', $team);
        $f3->set('avatar', $avatar);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Teammitglied anzeigen');
        $f3->set('mainHeading', 'Teammitglied anzeigen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/team-show.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Datensätze eines Teeammitgliedes ändern
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function teamEdit($f3, $params)
    {
        $tid = $params['tid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($tid, FILTER_VALIDATE_INT)) {
            $team = [];
        } else {
            $tm = new \Models\TeamModel();
            $team = $tm->teamShow($tid);
            $am = new \Models\AvatarModel();
            $avatar = $am->avatarShow($team[0]['team_id']);
        }
        $f3->set('team', $team);
        $f3->set('avatar', $avatar);

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Teammitglied ändern');
        $f3->set('mainHeading', 'Teammitglied ändern');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/team-edit.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Überschreibt die vorhanden Datensätze eines Teaammitgliedes
     * wird für POST verwendet
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function teamUpdate($f3, $params)
    {
        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'nachname'  => 'required|alpha_space,255',
                'vorname'   => 'required|alpha_space,255',
                'versNr'    => 'required',
                'gebTag'    => 'required',
                'adresse'   => 'required|max_len,255',
                'plz'   => 'required',
                'ort'   => 'required',
                'telefon'   => 'required',
                'email' => 'required',
                'dienstvertrag' => 'required',
                'gruppe'    => 'required',
                'funktion'  => 'required'
            ));
            $validData = $gump->run($_POST);
        }
      //  var_dump($params['kid']); exit();
        if (!filter_var($params['tid'], FILTER_VALIDATE_INT)) {
            $team = [];
        } else {
            $tm = new \Models\TeamModel();
            $f3->set('kind_id', $params['tid']);
            $team = $tm->teamEdit($validData, $params['tid']);
            $f3->reroute('/team');
        }

        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Teammitglied ändern');
        $f3->set('mainHeading', 'Teammitglied ändern');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/team-edit.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');
    
        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Teammitglied aus der DB löaschen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function teamDelete($f3, $params) {
        $tid = $params['tid'];
        // Nur Ganzzahlen sind erlaubt
        if (!filter_var($tid, FILTER_VALIDATE_INT)) {
            echo 'Error: Teammitglied konnte nicht gelöscht werden';
        } else {
            $tm = new \Models\TeamModel();
            if ($tm->teamDelete($tid)) {
                echo '1'; // Bei Erfolg wird 1 zurück gegeben
                $f3->reroute('/team');
            }
            else {
                echo 'Error';
            }
        }
    }
}
