<?php

namespace Controller;

use \Template;

class InboxController
{
    /**
     * Liefert die View für die Inbox 
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function inbox($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head-calendar.html');
        $f3->set('pageTitle', 'Inbox');
        $f3->set('mainHeading', 'Inbox');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/inbox.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Neue Nachricht senden 
     * wird für GET verwendet
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function nachricht($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head-calendar.html');
        $f3->set('pageTitle', 'Nachricht senden');
        $f3->set('mainHeading', 'Nachricht senden');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/nachricht.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Liefert die View, wenn eine Nachricht gesendet wurde
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function nachrichtGesendet($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head-calendar.html');
        $f3->set('pageTitle', 'Nachricht senden');
        $f3->set('mainHeading', 'Nachricht senden');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/nachricht-gesendet.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Liefert die View für die Nachrichtenansicht
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function nachrichtExample($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head-calendar.html');
        $f3->set('pageTitle', 'Nachricht senden');
        $f3->set('mainHeading', 'Nachricht senden');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/nachricht-example.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }
}
