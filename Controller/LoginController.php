<?php

namespace Controller;

use \Template;
use \Models\UserModel;

class LoginController extends IndexController
{
    /**
     * Liefert die View für den Login
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function index($f3, $params)
    {
        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'Login');
        $f3->set('mainHeading', 'Login');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', '/views/content/login.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Login Formular wird mit POST an AUTHENTICATE
     * gesendet & somit auf die DB zugegriffen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function authenticate($f3, $params)
    {

        $username = $f3->get('POST.username');
        $password = $f3->get('POST.password');
        $user = new \Models\UserModel($this->db);

        $user->getByName($username);

        if ($user->dry()) {
            $this->f3->reroute('/login');
        }

        if (password_verify($password, $user->password)) {
            $this->f3->set('SESSION.user', $user->username);
            $this->f3->reroute('/dashboard');
        } else {
            $this->f3->reroute('/login');
        }
    }

    /**
     * Liefert das Template
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function render($f3, $params)
    {
        $template = new Template;
        echo $template->render('login.html');
    }

    /**
     * Passwort Validierung für das Login
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function passwort($f3, $params)
    {

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'email'  => 'required|max_len,255',
            ));
            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            } else {
            }
        }
        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'Passwort');
        $f3->set('mainHeading', 'Passwort');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', '/views/content/passwort.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }
}
