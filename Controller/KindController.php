<?php

namespace Controller;

use \Template;

class KindController
{
    /**
     * Neues Kind anlegen
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function neuesKind($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Neues Kind anlegen');
        $f3->set('mainHeading', 'Neues Kind anlegen');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/neues-kind.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }

    /**
     * Validiert das Kindformular und ruft die Methode auf
     * um die validierten Daten in der Datenbank zu speichern
     *
     * @param Object $f3
     * @param Array $params
     * 
     */
    public function kindSpeichern($f3, $params)
    {

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'vorname'   => 'required|alpha_space,255',
                'nachname'  => 'required|alpha_space,255',
                'versNr'   => 'required|numeric|exact_len,4',
                'gebTag'   => 'required',
                'kundNr'   => 'required',
                'adresse'   => 'required|max_len,255',
                'plz'   => 'required|numeric|min_len,4',
                'ort'   => 'required|alpha_space,255',
                'muttersprache' => 'required|alpha_space,255',
                'geschwister'   =>  'max_len,255',
                'start' => 'required',
                'gruppe'    => 'required',
                'fruehbetreuung'    => 'numeric',
                'halbtags'  => 'numeric',
                'ganztags'  => 'numeric',
                'allergien' => 'alpha_space,255',
                'krankheiten'   => 'alpha_space,255',
                'windeln'   => 'numeric',
                'schnuller' => 'numeric'
            ));
            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            } else {
                $km = new \Models\KindModel();
                $isStored = $km->kinderdatenSpeichernDB($validData);

                if ($isStored > 0) {
                    $f3->set('alertSuccess', 'Kind erfolgreich gespeichert!');
                    $f3->set('UPLOADS', 'uploads/');
                    $overwrite = false;
                    $slug = true;

                    /* 
        fatfree hat eine Webklasse; hat unter anderem die MIME-Type Erkennung, die wir für fileuploads anwenden somit können. 
        */
                    $web = \Web::instance();
                    $files = $web->receive(
                        function ($file, $avatar) {
                            return true;
                        },
                        $overwrite,
                        $slug
                    );

                    $file = key($files);
                    $uploadId = $km->avatarSpeichernDB($file);
                    $f3->reroute('/kinder?success=ja');
                } else {
                    $f3->set('alertError', 'Fehler! Das Foto konnte nicht gespeichert werden.');
                }
            }
        }
    }
}
