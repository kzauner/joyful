<?php

namespace Controller;

use \Template;

class KalenderController
{
    /**
     * Liefert die view für den Kalender
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function index($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head-calendar.html');
        $f3->set('pageTitle', 'Kalender');
        $f3->set('mainHeading', 'Kalender');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/kalender.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Liefert die view um einen neuen Kalendereintrag zu speichern
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function termin($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head-calendar.html');
        $f3->set('pageTitle', 'Kalender');
        $f3->set('mainHeading', 'Kalender');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/termin.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }
}
