<?php

namespace Controller;

use \Template;

class UserController
{
    /**
     * Liefert die Bestandteile für das Dashboard
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function dashboard($f3, $params)
    {
        $f3->set('header', '/views/layouts/dashLayout-head.html');
        $f3->set('pageTitle', 'Dashboard');
        $f3->set('mainHeading', 'Dashboard');
        $f3->set('body', '/views/layouts/dashLayout-body.html');
        $f3->set('content', '/views/content/dashboard.html');
        $f3->set('footer', '/views/layouts/dashLayout-footer.html');

        echo Template::instance()->render('/views/dashIndex.html');
    }
}
