<?php

namespace Controller;

use \Template;

class MenuController
{
    /**
     * Liefert die View für die Eltern-Seite
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function eltern($f3, $params)
    {
        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'Eltern');
        $f3->set('mainHeading', 'Eltern');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', 'views/content/eltern.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Liefert die View für die KIGA-Seite
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kiga($f3, $params)
    {
        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'KIGA');
        $f3->set('mainHeading', 'Kiga');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', 'views/content/kiga.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Liefert die View für das Kontaktformular
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function kontakt($f3, $params)
    {

        if (!empty($_POST)) {
            $gump = new \GUMP('de');

            $gump->validation_rules(array(
                'betreff'   => 'required|alpha_space,255',
                'vorname'  => 'required|alpha_space,255',
                'nachname'  => 'required|alpha_space,255',
                'email'  => 'required|max_len,255',
                'telefon'  => 'numeric',
                'thema'  => 'required',
                'nachricht'  => 'required',
            ));
            $validData = $gump->run($_POST);

            if ($validData === false) {
                $errors = $gump->get_errors_array();
                $f3->set('errors', $errors);
                $f3->set('values', $_POST);
            } else {
            }
        }

        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'Kontaktformular');
        $f3->set('mainHeading', 'Kontakt');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', 'views/content/kontakt.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Öffnet die Erfolgsmeldung, wenn eine Nachricht gesendet wurde
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function erfolgreich($f3, $params)
    {
        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'Erfolgreich gesendet');
        $f3->set('mainHeading', 'Erfolgreich gesendet');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', 'views/content/erfolgreich.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }
}
