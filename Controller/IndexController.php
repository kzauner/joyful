<?php

namespace Controller;

use \Template;

class IndexController
{
    /**
     * Liefert die Bestandteile für die Startseite
     *
     * @param Object $f3
     * @param Array $params
     * @return Void
     */
    public function index($f3, $params)
    {
        $f3->set('header', '/views/layouts/layout-head.html');
        $f3->set('pageTitle', 'Startseite');
        $f3->set('mainHeading', 'Joyful Startseite');
        $f3->set('body', '/views/layouts/layout-body.html');
        $f3->set('content', '/views/content/home.html');
        $f3->set('footer', '/views/layouts/layout-footer.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * Afterroute
     *
     * @return Void
     */
    public function afterroute()
    {
    }

    /**
     * Konstruktor um die DB Verbindung bekannt zu geben
     */
    public function __construct()
    {
        $f3 = \Base::instance();
        $this->f3 = $f3;

        $db = new \DB\SQL('mysql:host=localhost;port=3306;dbname=joyful', 'joyfuluser', 'asdfjiwerijsdw3r');

        $this->db = $db;
    }
}
