<?php
$f3->route('GET /', 'Controller\IndexController->index');

/* Startseiten */
$f3->route('GET /eltern', 'Controller\MenuController->eltern');
$f3->route('GET /kiga', 'Controller\MenuController->kiga');
$f3->route('GET /kontakt', 'Controller\MenuController->kontakt');
$f3->route('POST /erfolgreich', 'Controller\MenuController->erfolgreich');

/* Login */
$f3->route('GET /login', 'Controller\LoginController->index');
$f3->route('POST /authenticate', 'Controller\LoginController->authenticate');
$f3->route('GET /passwort', 'Controller\LoginController->passwort');

/* Dashboard Menü */
$f3->route('GET /dashboard', 'Controller\UserController->dashboard');
$f3->route('GET /inbox', 'Controller\InboxController->inbox');
$f3->route('GET /nachricht', 'Controller\InboxController->nachricht');
$f3->route('POST /nachricht-gesendet', 'Controller\InboxController->nachrichtGesendet');
$f3->route('GET /nachricht-example', 'Controller\InboxController->nachrichtExample');
$f3->route('GET /kalender', 'Controller\KalenderController->index');
$f3->route('GET /termin', 'Controller\KalenderController->termin');
$f3->route('GET /speiseplan', 'Controller\SpeiseplanController->speiseplan');
$f3->route('GET /speiseplan/neu', 'Controller\SpeiseplanController->speiseplanNeu');

/* Kinder Ansichtsmenü */
$f3->route('GET /kinder', 'Controller\KinderController->kinder');
$f3->route('GET /kinder/group', 'Controller\KinderController->kinderGroup');
$f3->route('GET /kind/@kid', 'Controller\KinderController->kindShow');
$f3->route('GET /kind/@kid/edit', 'Controller\KinderController->kindEdit');
$f3->route('POST /kind/@kid/save', 'Controller\KinderController->kindUpdate');
$f3->route('GET /kind/@kid/delete', 'Controller\KinderController->kindDelete');

/* Kinder Akte */
$f3->route('GET /kind', 'Controller\KindController->neuesKind');
$f3->route('POST /kind/save', 'Controller\KindController->kindSpeichern');
$f3->route('GET /kontaktperson', 'Controller\KontaktController->neuerKontakt');
$f3->route('POST /kontaktperson/save', 'Controller\KontaktController->kontaktSpeichern');
$f3->route('GET /kontaktperson/@kid/show', 'Controller\KinderController->kontaktShow');

/* Team Akte */
$f3->route('GET /team', 'Controller\TeamController->team');
$f3->route('GET /team/neu', 'Controller\TeamController->teamNew');
$f3->route('POST /team/save', 'Controller\TeamController->teamNew');
$f3->route('GET /team/@tid', 'Controller\TeamController->teamShow');
$f3->route('GET /team/@tid/edit', 'Controller\TeamController->teamEdit');
$f3->route('POST /team/@tid/save', 'Controller\TeamController->teamUpdate');
$f3->route('GET /team/@tid/delete', 'Controller\TeamController->teamDelete');




