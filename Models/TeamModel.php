<?php

namespace Models;

class TeamModel extends Model
{
    /**
     * Alle Mitarbeiter ermitteln
     *
     * @return Array
     */
    public function team(): array
    {
        return $this->db->exec('SELECT * FROM team');
    }

    /**
     * neuen Mitarbeiter speichern
     *
     * @param Array $data
     * @return Void
     */
    public function teamSave(array $data)
    {
        $isStored = $this->db->exec('INSERT INTO team (nachname,vorname,versnr,gebtag,adresse,plz,ort,telefon,email,dienstvertrag,funktion_id,gruppen_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', [$data['nachname'], $data['vorname'], $data['versNr'], $data['gebTag'], $data['adresse'], $data['plz'], $data['ort'], $data['telefon'], $data['email'], $data['dienstvertrag'], $data['funktion'], $data['gruppe']]);
        return $data;
    }

    /**
     * Ein einzelnes Teammitglied ermitteln
     *
     * @param Integer $id
     * @return Array
     */
    public function teamShow(int $id): array
    {
        $team = $this->db->exec('SELECT * FROM team WHERE team_id=?', $id);

        if (count($team) === 0) {
            return [];
        }

        return $team;
    }
    /**
     * Ein Teammitglied updaten
     *
     * @return Boolean
     */
    public function teamEdit (array $data, int $team_id)
    {
        $team = $this->db->exec('UPDATE team SET nachname=?,vorname=?,versnr=?,gebtag=?,adresse=?,plz=?,ort=?,telefon=?,email=?,dienstvertrag=?,funktion_id=?,gruppen_id=? WHERE team_id=?' , [$data['nachname'], $data['vorname'], $data['versNr'], $data['gebTag'], $data['adresse'], $data['plz'], $data['ort'], $data['telefon'], $data['email'], $data['dienstvertrag'], $data['funktion'], $data['gruppe'], $team_id]);
        return $team;
    }

    /**
     * Ein Teammitglied löschen
     *
     * @param Integer $id
     * @return Void
     */
    public function teamDelete (int $id) 
    {
        $isDeleted = $this->db->exec('DELETE FROM team WHERE team_id=?', $id);
        return $isDeleted;
    }
}
