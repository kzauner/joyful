<?php

namespace Models;

class KinderModel extends Model
{
    /**
     * Alle Kinder ermitteln
     *
     * @return array
     */
    public function kinder(): array
    {
        return $this->db->exec('SELECT * FROM kind');
    }

    /**
     * Ein einzelnes Kind ermitteln
     *
     * @param Integer $id
     * @return Array
     */
    public function kind(int $id): array
    {
        $kinder = $this->db->exec('SELECT * FROM kind WHERE kind_id=?', $id);

        if (count($kinder) === 0) {
            return [];
        }

        return $kinder;
    }
    /**
     * Ein Kind updaten
     *
     * @return Boolean
     */
    public function kindEdit (array $data, int $kind_id)
    {
        $kinder = $this->db->exec('UPDATE kind SET nachname=?,vorname=?,versnr=?,gebtag=?,kundnr=?,adresse=?,plz=?,ort=?,muttersprachen=?,geschwister=?,starttag=?,fruehbetreuung=?,halbtags=?,ganztags=?,allergien=?,krankheiten=?,windeln=?,schnuller=?,gruppen_id=? WHERE kind_id=?' , [$data['nachname'], $data['vorname'], $data['versNr'], $data['gebTag'], $data['kundNr'], $data['adresse'], $data['plz'], $data['ort'], $data['muttersprache'], $data['geschwister'], $data['start'], $data['fruehbetreuung'], $data['halbtags'], $data['ganztags'], $data['allergien'], $data['krankheiten'], $data['windeln'], $data['schnuller'], $data['gruppe'], $kind_id]);
        return $kinder;
    }

    /**
     * Ein Kind löschen
     *
     * @param integer $id
     * @return void
     */
    public function kindDelete (int $id) 
    {
        $isDeleted = $this->db->exec('DELETE FROM kind WHERE kind_id=?', $id);
        return $isDeleted;
    }
}
