<?php

namespace Models;

class Avatarmodel extends Model 
{
    /**
     * Ein Avatar eines Kindes ermitteln
     *
     * @param Integer $id
     * @return Array
     */
    public function avatarShow(int $id): array
    {
        $avatar = $this->db->exec('SELECT * FROM dateien WHERE kind_id=?', $id);

        if (count($avatar) === 0) {
            return [];
        }

        return $avatar;
    }
}
