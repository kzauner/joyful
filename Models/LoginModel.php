<?php

namespace Models;

class User extends \DB\SQL\Mapper
{
    /**
     * DB Verbindung Herstellung
     *
     * @param \DB\SQL $db
     */
    public function __construct(\DB\SQL $db)
    {
        $db = new \DB\SQL('mysql:host=localhost;port=3306;dbname=joyful', 'joyfuluser', 'asdfjiwerijsdw3r');
        $user = new \DB\SQL\Mapper($db, 'user');
        $auth = new \Auth($user, array());
        $auth->basic();
    }

    /**
     * Alle Kinder ermitteln
     *
     * @return array
     */
    public function user(): array
    {
        return $this->db->exec('SELECT * FROM user');
    }
}
