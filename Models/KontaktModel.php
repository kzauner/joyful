<?php

namespace Models;

class KontaktModel extends Model
{
    /**
     * Kontaktperson in die Datenbank speichern
     *
     * @param Array $data
     * @return Boolean
     */
    public function kontaktpersonSpeichernDB(array $data)
    {
        $isStored = $this->db->exec('INSERT INTO kontaktperson (kind_id,beziehung,famstand,vorname,nachname,adresse,plz,ort,telefon,email,bevorzugt) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [$data['kind_id'],$data['beziehung'], $data['famstand'], $data['vorname'], $data['nachname'], $data['adresse'], $data['plz'], $data['ort'], $data['telefon'], $data['email'], $data['bevorzugt']]);
        return $this->db->lastInsertId();
    }

    /**
     * Zeigt den Kontakt zum jeweiligen Kind an.
     *
     * @param Integer $id
     * @return Array
     */
    public function kontaktShow(int $id): array
    {
        $kontakt = $this->db->exec('SELECT * FROM kontaktperson WHERE kind_id=?', $id);

        if (count($kontakt) === 0) {
            return [];
        }

        return $kontakt; 
    }
}