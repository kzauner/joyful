<?php

namespace Models;

class KindModel extends Model
{
    /**
     * Neues Kind in der Datenbank speichern
     *
     * @param Array 
     * @return Boolean
     */
    public function kinderdatenSpeichernDB(array $data)
    {
        $isStored = $this->db->exec('INSERT INTO kind (nachname,vorname,versnr,gebtag,kundnr,adresse,plz,ort,muttersprachen,geschwister,starttag,fruehbetreuung,halbtags,ganztags,allergien,krankheiten,windeln,schnuller,gruppen_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$data['nachname'], $data['vorname'], $data['versNr'], $data['gebTag'], $data['kundNr'], $data['adresse'], $data['plz'], $data['ort'], $data['muttersprache'], $data['geschwister'], $data['start'], $data['fruehbetreuung'], $data['halbtags'], $data['ganztags'], $data['allergien'], $data['krankheiten'], $data['windeln'], $data['schnuller'], $data['gruppe']]);
        return $this->db->lastInsertId();
    }

    /**
     * Uploads bzw. Dateien des Kindes in der Datenbank speichern
     *
     * @param String $data
     * @return Void
     */
    public function avatarSpeichernDB(string $data)
    {
        $isStored = $this->db->exec('INSERT INTO dateien (dateiname) VALUES (?)', [$data]);
        return $this->db->lastInsertID();
    }
}
