<?php
require_once 'vendor/autoload.php';

$f3 = \Base::instance();

$f3->config('config.ini');
require 'routes.php';

new Session();

$f3->run(); 