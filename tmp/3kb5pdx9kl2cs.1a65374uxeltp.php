<div class="container col s12 m4 l5">
    <?php echo $this->render('views/modules/alerts.html',NULL,get_defined_vars(),0); ?>
      <div class="row">
        <form class="col s12" method="POST" action="/kontaktperson/save">
          
          <h4>Kontaktpersonen</h4>
          <br>

          <div class="row">
          <div class="input-field col s3">
            <select name="kind_id">
              <?php foreach (($result?:[]) as $kind): ?>
                <option value="<?= ($kind['kind_id']) ?>"><?= ($kind['nachname']) ?></option>
              <?php endforeach; ?>
            </select>
          <label>Kind auswählen</label>
          </div></div>

          <div class="row">
              <div class="input-field col s3">
                <input id="beziehung" name="beziehung" type="text" class="validate" />    
                <?php if ($errors['beziehung']): ?>
                  <div class="field-error"><?= ($errors['beziehung']) ?></div>
                <?php endif; ?>          
                <label for="beziehung">Beziehung</label>
              </div>
              <div class="row">
                <div class="input-field col s3">
                  <input id="famstand" name="famstand" type="text" class="validate" />
                  <?php if ($errors['famstand']): ?>
                    <div class="field-error"><?= ($errors['famstand']) ?></div>
                  <?php endif; ?>
                  <label for="famstand">Familienstand</label>
                </div>
              </div>
            </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="nachname" name="nachname" type="text" class="validate" />    
              <?php if ($errors['nachname']): ?>
                <div class="field-error"><?= ($errors['nachname']) ?></div>
              <?php endif; ?>          
              <label for="nachname">Nachname</label>
            </div>
            <div class="row">
              <div class="input-field col s3">
                <input id="vorname" name="vorname" type="text" class="validate" />
                <?php if ($errors['vorname']): ?>
                  <div class="field-error"><?= ($errors['vorname']) ?></div>
                <?php endif; ?>
                <label for="vorname">Vorname</label>
              </div>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
              <input id="adresse" name="adresse" type="text" class="validate" />
              <?php if ($errors['adresse']): ?>
                <div class="field-error"><?= ($errors['adresse']) ?></div>
              <?php endif; ?>
              <label for="adresse">Adresse</label>          
            </div>
          </div>
    
          <div class="row">
            <div class="col s12">
              <div class="row">
                <div class="input-field col s3">
                  <input id="plz" name="plz" type="text" class="validate" />
                  <?php if ($errors['plz']): ?>
                    <div class="field-error"><?= ($errors['plz']) ?></div>
                  <?php endif; ?>
                  <label for="plz">PLZ</label>
                </div>
                <div class="input-field col s3">
                  <input id="ort" name="ort"  type="text" class="validate" />
                  <?php if ($errors['ort']): ?>
                    <div class="field-error"><?= ($errors['ort']) ?></div>
                  <?php endif; ?>
                  <label for="ort">Ort</label>
                </div>              
              </div>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
              <input id="telefon" name="telefon" type="text" class="validate" />
              <?php if ($errors['telefon']): ?>
                <div class="field-error"><?= ($errors['telefon']) ?></div>
              <?php endif; ?>
              <label for="telefon">Telefon</label>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
              <textarea id="email" name="email" class="materialize-textarea"></textarea>
              <?php if ($errors['email']): ?>
                <div class="field-error"><?= ($errors['email']) ?></div>
              <?php endif; ?>
              <label for="email">E-Mail</label>
            </div>
          </div>
  
          <br>
          <button class="btn waves-effect waves-light" type="submit" name="speichern">Speichern</button>
  
        </form>
      </div>
    </div>
    
<script>
      $(document).ready(function() {
        $("select").formSelect();
      });
</script> 
    