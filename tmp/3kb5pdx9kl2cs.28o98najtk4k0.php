<h3 class="container col s12 m4 l10">Speiseplan</h3>
<br />
<div class="container col s12 m4 l10">
  <a
    class="waves-effect waves-light btn-large"
    href="http://joyful.loc/speiseplan/neu"
    >Neuer Speiseplan</a
  >
</div>
<br />
<table class="centered container col s12 m4 l10">
  <thead>
    <tr>
      <th>
        Montag <br />
        10.03.
      </th>
      <th>
        Dienstag <br />
        11.03.
      </th>
      <th>
        Mittwoch <br />
        12.03.
      </th>
      <th>
        Donnerstag <br />
        13.03.
      </th>
      <th>
        Freitag <br />
        14.03.
      </th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Gemüsecremesuppe</td>
      <td>Grießnockerlsuppe</td>
      <td>Frittatensuppe</td>
      <td>Bohnensuppe</td>
      <td>Brokkolicremesuppe</td>
    </tr>
    <tr>
      <td>Hühnerschnitzel mit Pommes</td>
      <td>Spaghetti Bolognese</td>
      <td>Fischstäbchen mit Erbsenreis</td>
      <td>Gemüselaibchen mit Kartoffelpürree</td>
      <td>Kabeljaufilet mit Kroketten</td>
    </tr>
    <tr>
      <td>Topfengolatsche</td>
      <td>Vanillepudding</td>
      <td>Erdbeertörtchen</td>
      <td>Obstteller</td>
      <td>Schokoladenpudding</td>
    </tr>
  </tbody>
</table>
