<h3 class="container col s12 m4 l10">
  Kinder
  <a href="http://joyful.loc/kinder/group">
    <i class="small material-icons black-text">apps</i>
  </a>
</h3>
<br />

<div class="container col s12 m4 l12">
  <a class="waves-effect waves-light btn-large" href="http://joyful.loc/kind"
    >Neues Kind</a
  >
  <a class="waves-effect waves-light btn-large" href="http://joyful.loc/kontaktperson"
    >Neue Kontaktperson</a
  >
</div> 


<br />
<table class="responsive-table highlight container col s12 m4 l10">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nachname</th>
      <th>Vorname</th>
      <th>Gruppe</th>
      <th>Aktion</th>
      <th>Kontaktperson</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach (($resultKind?:[]) as $kind): ?>
      <tr>
        <td><?= ($kind['kind_id']) ?></td>
        <td><?= ($kind['nachname']) ?></td>
        <td><?= ($kind['vorname']) ?></td>
        <td><?= ($kind['gruppen_id']) ?></td>
        <td>
          <a href="/kind/<?= ($kind['kind_id']) ?>"
            ><i class="small material-icons black-text">visibility</i></a
          >
          <a href="/kind/<?= ($kind['kind_id']) ?>/edit"
            ><i class="small material-icons black-text">create</i></a
          >
          <a href="/kind/<?= ($kind['kind_id']) ?>/delete" onclick="alert('Sind Sie sich sicher, dass Sie dieses Kind löschen wollen? Dieser Schritt kann nicht mehr rückgangig gemacht werden.');"><i class="small material-icons black-text">clear</i></a>
        </td>
        <td><a href="/kontaktperson/<?= ($kind['kind_id']) ?>/show">Anzeigen</a></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
