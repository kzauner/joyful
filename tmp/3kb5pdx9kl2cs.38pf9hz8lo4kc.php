<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <h1 class="header center orange-text">
      <img class="responsive-img logo_index" src="/img/joyful_logo.svg" />
    </h1>
    <div class="row center">
      <h4 class="header col s12 light">Die App für den Kindergarten</h4>
    </div>
  </div>
</div>
<div class="section no-pad-bot" id="index-banner">
  <img src="/img/annabelle.jpg" alt="" style="max-width: 100%;" />
</div>

<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center black-text">
            <i class="material-icons">business_center</i>
          </h2>
          <h5 class="center">KIGA</h5>

          <p class="light justified">
            joyful verbindet Erzieher, Eltern, Träger und Kinder mittels einer
            unkomplizierten, übersichtlichen und selbsterklärenden
            Kommunikationszentrale. joyful ist eine sichere und webbasierte
            Kindergarten Applikation, die es Direktoren und Pädagogen
            ermöglicht, Ihr Datenmanagement zeitgemäß und sicher zu gestalten
            sowie Eltern effektiv auf dem Laufenden zu halten.
          </p>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center black-text">
            <i class="material-icons">child_care</i>
          </h2>
          <h5 class="center">Eltern</h5>

          <p class="light justified">
            joyful ermöglicht einen Kontaktaustausch zwischen Ihnen und dem
            Kindergarten. Vergessen Sie das Wirrwarr vom schwarzen Brett, E-Mail
            & Co, indem joyful alle wichtigen Informationen und Veranstaltungen
            an einem Ort bündelt. Krankmeldungen, Abwesenheiten oder
            Kontaktpersonen können sofort mit nur wenigen Mausklicks erledigt
            werden.
          </p>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center black-text">
            <i class="material-icons">account_circle</i>
          </h2>
          <h5 class="center">Über Joyful</h5>

          <p class="light justified">
            Joyful wurde im Zuge meiner Ausbildung zur Web Developerin, als
            Abschlussprojekt gewählt. Die Idee von joyful beruht auf einer
            persönlichen Herzensangelegenheit, da ich, Katharina Zauner, als
            Mutter von 2 Töchtern den Alltagsstress mit Kindern sehr gut
            nachvollziehen kann und somit mir eine Erleichterung gewünscht habe.
          </p>
        </div>
      </div>
    </div>
  </div>
  <br /><br />
</div>
