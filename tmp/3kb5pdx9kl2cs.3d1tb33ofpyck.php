<div class="container col s12 m4 l5">
    <?php echo $this->render('views/modules/alerts.html',NULL,get_defined_vars(),0); ?>
      <div class="row">
        <form class="col s12" method="POST" action="/kinder/save/3">

        <h4><i>Schritt 3 von 3:</i> Dateien</h4>
        <br>

        <h5>Entwicklungsprotokolle</h5>
        <br>
        <label for="protokoll">Lade ein neues Protokoll hoch:</label>
        <input type="file"
       id="protokoll" name="protokoll"
       accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
        <br>
        <br>

        <h5>Dateien</h5>
        <br>
        <label for="ecard">E-Card:</label>
        <input type="file"
               id="ecard" name="ecard"
               accept="image/png, image/jpeg">

        <br><br>

        <label for="reisepass">Reisepass:</label>
        <input type="file"
               id="reisepass" name="reisepass"
               accept="image/png, image/jpeg">

        <br><br>

        <label for="kundnr">Kundinnen-Nr:</label>
        <input type="file"
               id="kundnr" name="kundnr"
               accept="image/png, image/jpeg">           
        
        <br><br>
        
        <button class="btn waves-effect waves-light" type="submit" name="speichern">Speichern</button>
        </form>
       </div>
</div>