<h3 class="seitenabstand">joyful für den KIGA</h3>
<br />
<img class="responsive-img seitenabstand browser" src="/img/browser.png" />
<p class="justified seitenabstand">
  joyful optimiert für Sie als Erzieher alle alltäglichen Prozesse,
  Dokumentationen und Angebote. Mit diesem intuitiven Programm, welches bloß
  eine intakte Internetverbindung benötigt und <b>nicht installiert</b> werden
  muss, werden Aufgaben von A bis Z mit nur wenigen Mausklicken erledigt – und
  dies ortsungebunden. Erledigen Sie Ihre Bürokratie auf dem Spielplatz, im
  Gruppenraum oder im Pausenraum. <br />
  Vergessen Sie das Wirrwarr vom schwarzen Brett, E-Mail & Co, indem joyful alle
  wichtigen Informationen und Veranstaltungen an einem Ort bündelt. Auch die
  Kinderverwaltung, der Kern jedes Kindergartens, war noch nie so einfach. Alle
  wichtigen Daten wie Kontaktpersonen, Abholzeiten, Unverträglichkeiten oder
  Allergien sind
  <b>jederzeit griffbereit.</b> <br />
  Sparen Sie sich in der Früh die Zeit mit Anrufbeantwortern abhören und
  Nachrichten lesen, denn Krankmeldungen oder Verspätungen werden übersichtlich
  auf Ihrer Benutzeroberfläche angezeigt. Mit joyful erweitern Sie somit Ihren
  Freiraum auch bei der Kommunikation mit Eltern. Senden Sie ganz einfach
  Nachrichten, Bestätigungen, Rechnungen oder auch Fotos an die jeweiligen
  Personen bzw. Gruppen, und lösen Sie auf diese Weise endlose
  Kommunikationsschleifen. <br />
  Durch unsere unkomplizierte und selbsterklärende Bedienung sparen Sie sich mit
  Ihrem Team viel Zeit und ermöglichen auch den Eltern eine direkte
  Kommunikation mit Ihrer Einrichtung oder auch untereinander. In
  <b>Echtzeit</b> können Eltern spannendes aus dem Alltag ihrer Kinder erfahren,
  sowie von Ihnen bereit gestellte Dokumente oder Fotos direkt herunterladen –
  und natürlich Dringlichkeiten wie Krankheiten sofort und unkompliziert melden.
  <br /><br />

  <b>Datenschutz ist joyful wichtig!</b> Datenschutz und Datensicherheit liegen
  uns sehr am Herzen. Bei joyful sind die Daten sicher. joyful erfüllt höchste
  Datenschutzstandards nach den Vorgaben der EU-Datenschutz-Grundverordnung
  (DSGVO). Das Daten- und Sicherheitskonzept wird ständig den aktuellsten
  semantischen und technischen Anforderungen angepasst. Unser mehrfach
  ausgezeichneter deutscher Hosting-Partner betreibt seine Rechenzentren in
  Österreich und diese werden rund um die Uhr überwacht.
</p>
<br />
<hr />
<h4 class="seitenabstand">Kosten</h4>

<table class="kosten seitenabstand centered">
  <thead>
    <tr class="teal lighten-5">
      <th><h5>MINI</h5></th>
      <th><h5>MIDI</h5></th>
      <th><h5>MAXI</h5></th>
    </tr>
  </thead>

  <tbody>
    <tr class="grey lighten-3">
      <td>bis 15 Kindern</td>
      <td>ab 16 bis 50 Kindern</td>
      <td>ab 51 Kindern</td>
    </tr>
    <tr class="pink lighten-4">
      <td>15,00 €</td>
      <td>39,00 €</td>
      <td>49,00 €</td>
    </tr>
    <tr class="grey lighten-3">
      <td>Preis pro Monat, inkl. MwSt.</td>
      <td>Preis pro Monat, inkl. MwSt.</td>
      <td>Preis pro Monat, inkl. MwSt.</td>
    </tr>
  </tbody>
</table>
<br />
<hr />
