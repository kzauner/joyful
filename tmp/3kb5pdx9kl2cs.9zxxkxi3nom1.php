<div class="container col s12 m4 l5">
  <div class="row">
    <form class="col s12" method="POST">
      <h4>Neuer Kalendereintrag</h4>
      <br />
      <div class="row">
        <div class="input-field col s6">
          <input id="titel" name="titel" type="text" class="validate" />
          <label for="titel">Titel</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s3">
          <input id="datum" name="datum" type="text" class="datepicker" />
          <label for="datum">Datum</label>
        </div>
        <div class="row">
        <div class="input-field col s3">
          <input type="text" class="timepicker" />
          <label for="uhrzeit">Uhrzeit</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s6">
          <textarea
            id="beschreibung"
            name="beschreibung"
            class="materialize-textarea"
          ></textarea>
          <label for="beschreibung">Beschreibung</label>
        </div>
      </div>

      <button
        class="btn waves-effect waves-light"
        type="submit"
        name="speichern"
      >
        Speichern
      </button>
    </form>
  </div>
</div>

<script>
  $(document).ready(function() {
    $(".datepicker").datepicker({ format: "yyyy-mm-dd" });
  });
  
  $(document).ready(function(){
    $('.timepicker').timepicker();
  });
</script>
