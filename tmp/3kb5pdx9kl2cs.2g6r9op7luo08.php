<h3 class="container col s12 m4 l10">Inbox</h3>
<br />

<div class="container col s12 m4 l10">
  <a
    class="waves-effect waves-light btn-large"
    href="http://joyful.loc/nachricht"
    >Neue Nachricht</a
  >
</div>

<br />

<ul class="collection container col s12 m4 l10">
  <li class="collection-item avatar">
    <img src="/img/mustermann.jfif" alt="" class="circle">
    <span class="title"><a href="http://joyful.loc/nachricht-example" class="black-text">Hans Mustermann</a></span>
    <p>Hans Mustermann
    </p>
    <a href="http://joyful.loc/nachricht-example" class="secondary-content"><i class="material-icons">email</i></a>
  </li>
  <li class="collection-item avatar">
    <img src="/img/rotzkind.jpg" alt="" class="circle">
    <span class="title">Heidi Lala</span>
    <p>Verspätung
    </p>
    <a href="#!" class="secondary-content"><i class="material-icons">email</i></a>
  </li>
  <li class="collection-item avatar">
    <img src="/img/harrypotter.jpg" alt="" class="circle">
    <span class="title">Harry Potter</span>
    <p>Krank
    </p>
    <a href="#!" class="secondary-content"><i class="material-icons">email</i></a>
  </li>
  <li class="collection-item avatar">
    <img src="/img/pippi.jpg" alt="" class="circle">
    <span class="title">Pippi Langstrumpf</span>
    <p>Krankmeldung
    </p>
    <a href="#!" class="secondary-content"><i class="material-icons">email</i></a>
  </li>
</ul>
          
