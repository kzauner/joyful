<div class="container col s12 m4 l5">
    <?php echo $this->render('views/modules/alerts.html',NULL,get_defined_vars(),0); ?>
    <div class="row">
      <form class="col s12" method="POST" action="/team/save">
  
        <h4>Neues Teammitglied</h4>
          <br>
  
        <!-- Avatar -->
        <div class="container col s12 m4 l10">
          <label for="avatar">Profilfoto (1x1):</label>
          <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">
        </div>
    
        <br>
        <br>
  
        <div class="row">
          <div class="input-field col s3">
            <input id="nachname" name="nachname" type="text" class="validate" />    
            <?php if ($errors['nachname']): ?>
              <div class="field-error"><?= ($errors['nachname']) ?></div>
            <?php endif; ?>          
            <label for="nachname">Nachname</label>
          </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="vorname" name="vorname" type="text" class="validate" />
              <?php if ($errors['vorname']): ?>
                <div class="field-error"><?= ($errors['vorname']) ?></div>
              <?php endif; ?>
              <label for="vorname">Vorname</label>
            </div>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s3">
            <input id="versNr" name="versNr" type="text" class="validate" />
            <?php if ($errors['versNr']): ?>
              <div class="field-error"><?= ($errors['versNr']) ?></div>
            <?php endif; ?>
            <label for="versNr">Vers. Nr.</label>
          </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="gebTag" name="gebTag" type="text" class="datepicker" />   
              <?php if ($errors['gebTag']): ?>
                <div class="field-error"><?= ($errors['gebTag']) ?></div>
              <?php endif; ?>           
              <label for="gebTag">Geburtstag</label>
            </div>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s6">
            <input id="adresse" name="adresse" type="text" class="validate" />
            <?php if ($errors['adresse']): ?>
              <div class="field-error"><?= ($errors['adresse']) ?></div>
            <?php endif; ?>
            <label for="adresse">Adresse</label>          
          </div>
        </div>
  
        <div class="row">
          <div class="col s12">
            <div class="row">
              <div class="input-field col s3">
                <input id="plz" name="plz" type="text" class="validate" />
                <?php if ($errors['plz']): ?>
                  <div class="field-error"><?= ($errors['plz']) ?></div>
                <?php endif; ?>
                <label for="plz">PLZ</label>
              </div>
              <div class="input-field col s3">
                <input id="ort" name="ort"  type="text" class="validate" />
                <?php if ($errors['ort']): ?>
                  <div class="field-error"><?= ($errors['ort']) ?></div>
                <?php endif; ?>
                <label for="ort">Ort</label>
              </div>              
            </div>
          </div>
        </div>
  
        <div class="row">
            <div class="input-field col s6">
              <input id="telefon" name="telefon" type="text" class="validate" />
              <?php if ($errors['telefon']): ?>
                <div class="field-error"><?= ($errors['telefon']) ?></div>
              <?php endif; ?>
              <label for="telefon">Telefon</label>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
              <textarea id="email" name="email" class="materialize-textarea"></textarea>
              <?php if ($errors['email']): ?>
                <div class="field-error"><?= ($errors['email']) ?></div>
              <?php endif; ?>
              <label for="email">E-Mail</label>
            </div>
          </div>
  
        <div class="row">
          <div class="input-field col s3">
            <input id="dienstvertrag" name="dienstvertrag" type="text" class="datepicker" />
            <?php if ($errors['dienstvertrag']): ?>
              <div class="field-error"><?= ($errors['dienstvertrag']) ?></div>
            <?php endif; ?>
            <label for="dienstvertrag">Start</label>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s3" id="gruppe">
            <select name="gruppe">
              <?php if ($errors['gruppe']): ?>
                <div class="field-error"><?= ($errors['gruppe']) ?></div>
              <?php endif; ?>
              <option value="1">Gruppe 1</option>
              <option value="2">Gruppe 2</option>
              <option value="3">Gruppe 3</option>
              <option value="4">Gruppe 4</option>
            </select>
            <label for="gruppe">Gruppe</label>
          </div>
        </div>
          
        <div class="row">
            <div class="input-field col s3" id="funktion">
              <select name="funktion">
                <?php if ($errors['funktion']): ?>
                  <div class="field-error"><?= ($errors['funktion']) ?></div>
                <?php endif; ?>
                <option value="1">Erzieher</option>
                <option value="2">Assistenz</option>
                <option value="3">Leitung</option>
                <option value="4">Springer</option>
              </select>
              <label for="funktion">Funktion</label>
            </div>
          </div>
       
        <button class="btn waves-effect waves-light" type="submit" name="speichern">Speichern</button>
      </form>
    </div>
  </div>
  
  
  <script>
    $(document).ready(function() {
      $(".datepicker").datepicker({format:"yyyy-mm-dd"});
    });
  
    $(document).ready(function() {
      $("select").formSelect();
    });
  </script> 
  