<div class="container col s12 m4 l10 offset-m2 l6 offset-l3">
  <div class="card-panel grey lighten-5 z-depth-1">
    <div class="row valign-wrapper">
      <div class="col s2">
        <img src="/img/mustermann.jfif" alt="" class="circle responsive-img" />
      </div>
      <div class="col s10">
        <span class="black-text">
          <h6>Hans Mustermann</h6>
          <h6>Speiseplan</h6>
          <p>12. März 2020, 13:12 Uhr</p>
          <a
            class="waves-effect waves-light btn-large"
            href="http://joyful.loc/inbox"
            >Zurück</a
          >
          <a
            class="waves-effect waves-light btn-large"
            href="http://joyful.loc/nachricht"
            >Antworten</a
          >
          <p>
            Lieber Albus! Hast du eine aktuelle Liste der Allergenen für den
            Speiseplan? Außerdem bitte ich um einen neuen Hilfskoch. Die Infos
            und Anforderungen die ich benötige, sende ich dir demnächst. Danke
            mfg
          </p>
        </span>
      </div>
    </div>
  </div>
</div>
