<h3 class="seitenabstand">Passwort zurücksetzen</h3>

<p class="seitenabstand">
  Bitte tragen Sie Ihre registrierte E-Mail Adresse ein. <br />
  In Kürze erhalten Sie eine E-Mail mit einem Link, <br />
  mit dem Sie Ihr Passwort zurücksetzen können.
</p>

<form action="/erfolgreich" method="post" class="col s6 seitenabstand">
  <div class="row">
    <div class="input-field col s6">
      <input id="username" name="username" type="email" class="validate" />
      <label for="username">E-Mail</label>
    </div>
  </div>

  <p>
    E-Mail vergessen? Schicken Sie uns eine Nachricht <br />
    über das <a href="http://joyful.loc/kontakt">Kontaktformular</a>.
  </p>

  <div class="login-submit">
    <button class="btn waves-effect waves-light" type="submit" name="senden">
      Senden
    </button>
  </div>
</form>
<br>
<br>
