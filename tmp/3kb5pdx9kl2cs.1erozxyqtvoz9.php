<div class="container col s12 m4 l5">
    <?php echo $this->render('views/modules/alerts.html',NULL,get_defined_vars(),0); ?>
      <div class="row">
        <form class="col s12" method="POST" action="/kontaktperson/save">
          
          <h4>Kontaktperson</h4>
          <br>

          <div class="row">
              <div class="input-field col s3">
                <input id="beziehung" name="beziehung" type="text" class="validate" value="<?= ($kontaktperson[0]['beziehung']) ?>"/>          
                <label for="beziehung">Beziehung</label>
              </div>
              <div class="row">
                <div class="input-field col s3">
                  <input id="famstand" name="famstand" type="text" class="validate" value="<?= ($kontaktperson[0]['famstand']) ?>"/>
                  <label for="famstand">Familienstand</label>
                </div>
              </div>
            </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="nachname" name="nachname" type="text" class="validate" value="<?= ($kontaktperson[0]['nachname']) ?>"/>        
              <label for="nachname">Nachname</label>
            </div>
            <div class="row">
              <div class="input-field col s3">
                <input id="vorname" name="vorname" type="text" class="validate" value="<?= ($kontaktperson[0]['vorname']) ?>"/>
                <label for="vorname">Vorname</label>
              </div>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
              <input id="adresse" name="adresse" type="text" class="validate" value="<?= ($kontaktperson[0]['adresse']) ?>"/>
              <label for="adresse">Adresse</label>          
            </div>
          </div>
    
          <div class="row">
            <div class="col s12">
              <div class="row">
                <div class="input-field col s3">
                  <input id="plz" name="plz" type="text" class="validate" value="<?= ($kontaktperson[0]['plz']) ?>"/>
                  <label for="plz">PLZ</label>
                </div>
                <div class="input-field col s3">
                  <input id="ort" name="ort"  type="text" class="validate" value="<?= ($kontaktperson[0]['ort']) ?>"/>
                  <label for="ort">Ort</label>
                </div>              
              </div>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
              <input id="telefon" name="telefon" type="text" class="validate" value="<?= ($kontaktperson[0]['telefon']) ?>"/>
              <label for="telefon">Telefon</label>
            </div>
          </div>
    
          <div class="row">
            <div class="input-field col s6">
                <input id="email" name="email" type="text" class="validate" value="<?= ($kontaktperson[0]['email']) ?>"/>
                <label for="email">E-Mail</label>
            </div>
          </div>
        </form>
      </div>
    </div>
    
<script>
      $(document).ready(function() {
        $("select").formSelect();
      });
</script> 
    