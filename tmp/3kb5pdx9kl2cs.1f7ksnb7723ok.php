<!DOCTYPE html>
<html lang="de">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1.0"
    />
    <title>Joyful | <?= ($pageTitle) ?></title>

    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      href="/css/materialize.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
    <link
      href="/css/style.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />

    <link href='/fullcalendar/packages/core/main.css' rel='stylesheet' />
    <link href='/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
    <script src='/fullcalendar/packages/core/main.js'></script>
    <script src='/fullcalendar/packages/daygrid/main.js'></script>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/js/materialize.js"></script>
    <script src="/js/calendar-init.js"></script>
    <script src="/js/init.js"></script>

   <!--  <script>

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid' ]
        });

        calendar.render();
      });

    </script> -->

  </head>

