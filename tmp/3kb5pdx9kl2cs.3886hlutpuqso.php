<h5 class="container col s12 m4 l10">Nachricht erfolgreich versendet!
</h5>
<br>
<br>
<div class="container col s12 m4 l10">
    <a class="waves-effect waves-light btn-large" href="http://joyful.loc/nachricht">Neue Nachricht</a>
  </div>
  
  <br>
  <table class="responsive-table highlight container col s12 m4 l10">
    <thead>
      <tr>
          <th>Absender</th>
          <th>Betreff</th>
      </tr>
    </thead>
  
    <tbody>
      <tr>
        <td>Hans Mustermann</td>
        <td>Speiseplan</td>
      </tr>
      <tr>
        <td>Maria Langhals</td>
        <td>Anfrage</td>
      </tr>
      <tr>
        <td>Severus Snape</td>
        <td>Anfrage</td>
      </tr>
      <tr>
        <td>Albert Einstein</td>
        <td>Reservierung</td>
      </tr>
    </tbody>
  </table>
  

