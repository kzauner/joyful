<h3 class="seitenabstand">Sende uns eine Nachricht.</h3>

<div class="row seitenabstand">
  <form action="/erfolgreich" method="post" class="col s9">
    <div class="row">
      <div class="input-field col s12">
        <input id="betreff" name="betreff" type="text" class="validate" />
        <label for="betreff">Betreff</label>
      </div>

      <div class="input-field col s6">
        <input id="vorname" name="vorname" type="text" class="validate" />
        <label for="vorname">Vorname</label>
      </div>

      <div class="input-field col s6">
        <input id="nachname" name="nachname" type="text" class="validate" />
        <label for="nachname">Nachname</label>
      </div>

      <div class="input-field col s6">
        <input id="email" name="email" type="email" class="validate" />
        <label for="email">Email</label>
      </div>

      <div class="input-field col s6">
        <input
          placeholder="optional"
          id="telefon"
          type="text"
          class="validate"
          name="telefon"
        />
        <label for="telefon">Telefon</label>
      </div>

      <div class="input-field col s12">
        <select name="thema">
          <option value="" disabled selected>Bitte wählen Sie ein Thema</option>
          <option value="1">Allgemeines</option>
          <option value="2">Benutzerkonto</option>
          <option value="3">Finanzielles</option>
          <option value="3">Beschwerde, Lob</option>
        </select>
        <label>Thema</label>
      </div>

      <div class="input-field col s12">
        <textarea
          id="textarea1"
          name="nachricht"
          class="materialize-textarea"
        ></textarea>
        <label for="textarea1">Ihre Nachricht</label>
      </div>

      <button class="btn waves-effect waves-light" type="submit" name="action">
        Senden
        <i class="material-icons right">send</i>
      </button>
    </div>
  </form>
</div>

<script>
  $(document).ready(function() {
    $("select").formSelect();
  });
</script>
