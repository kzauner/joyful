<h3 class="container col s12 m4 l10">
  Kinder
  <a href="http://joyful.loc/kinder">
    <i class="small material-icons">apps</i>
  </a>
</h3>
<br />
<div class="container col s12 m4 l2">
  <a
    class="waves-effect waves-light btn-large"
    href="http://joyful.loc/kinder/neu"
    >Neues Kind</a
  >
</div>
<br />
<div class="container col s12 m4 l10">
  <ul class="collapsible ">
    <li>
      <div class="collapsible-header">Gruppe 1</div>
      <div class="collapsible-body">
        <table class="responsive-table highlight">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nachname</th>
              <th>Vorname</th>
              <th>Gruppe</th>
              <th>Kontaktperson 1</th>
              <th>Telefon</th>
              <th>Kontaktperson 2</th>
              <th>Telefon</th>
              <th>Aktion</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>1</td>
              <td>Langstrumpf</td>
              <td>Pippi</td>
              <td>2</td>
              <td>Langstrumpf Heidi</td>
              <td>0660 547 5577</td>
              <td>Langstrumpf Peter</td>
              <td>0676 4567 6748</td>
              <td>
                <a href="#!"><i class="small material-icons">visibility</i></a>
                <a href="#!"><i class="small material-icons">create</i></a>
                <a href="#!"><i class="small material-icons">clear</i></a>
              </td>
            </tr>
            <tr>
              <td>1</td>
              <td>Langstrumpf</td>
              <td>Pippi</td>
              <td>2</td>
              <td>Langstrumpf Heidi</td>
              <td>0660 547 5577</td>
              <td>Langstrumpf Peter</td>
              <td>0676 4567 6748</td>
              <td>
                <a href="#!"><i class="small material-icons">visibility</i></a>
                <a href="#!"><i class="small material-icons">create</i></a>
                <a href="#!"><i class="small material-icons">clear</i></a>
              </td>
            </tr>
            <tr>
              <td>1</td>
              <td>Langstrumpf</td>
              <td>Pippi</td>
              <td>2</td>
              <td>Langstrumpf Heidi</td>
              <td>0660 547 5577</td>
              <td>Langstrumpf Peter</td>
              <td>0676 4567 6748</td>
              <td>
                <a href="#!"><i class="small material-icons">visibility</i></a>
                <a href="#!"><i class="small material-icons">create</i></a>
                <a href="#!"><i class="small material-icons">clear</i></a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </li>
    <li>
      <div class="collapsible-header">Gruppe 2</div>
      <div class="collapsible-body">
        <span>Lorem ipsum dolor sit amet.</span>
      </div>
    </li>
    <li>
      <div class="collapsible-header">Gruppe 3</div>
      <div class="collapsible-body">
        <span>Lorem ipsum dolor sit amet.</span>
      </div>
    </li>
    <li>
      <div class="collapsible-header">Gruppe 4</div>
      <div class="collapsible-body">
        <span>Lorem ipsum dolor sit amet.</span>
      </div>
    </li>
  </ul>
</div>

<script>
  $(document).ready(function() {
    $(".collapsible").collapsible();
  });
</script>
