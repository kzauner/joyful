<div class="container col s12 m4 l5">
    <?php echo $this->render('views/modules/alerts.html',NULL,get_defined_vars(),0); ?>
    <div class="row">
      <form class="col s12" method="POST" action="/kind/save" enctype="multipart/form-data">
  
        <h4>Neues Kind speichern</h4>
          <br>
  
        <!-- Avatar -->
        <div class="container col s12 m4 l10">
          <label for="avatar">Profilfoto (quadratisch):</label>
          <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">
        </div>
    
        <br>
        <br>
        <div class="row">
          <div class="input-field col s3">
            <input id="nachname" name="nachname" type="text" class="validate" />    
            <?php if ($errors['nachname']): ?>
              <div class="field-error"><?= ($errors['nachname']) ?></div>
            <?php endif; ?>          
            <label for="nachname">Nachname</label>
          </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="vorname" name="vorname" type="text" class="validate" />
              <?php if ($errors['vorname']): ?>
                <div class="field-error"><?= ($errors['vorname']) ?></div>
              <?php endif; ?>
              <label for="vorname">Vorname</label>
            </div>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s3">
            <input id="versNr" name="versNr" type="text" class="validate" />
            <?php if ($errors['versNr']): ?>
              <div class="field-error"><?= ($errors['versNr']) ?></div>
            <?php endif; ?>
            <label for="versNr">Vers. Nr.</label>
          </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="gebTag" name="gebTag" type="text" class="datepicker" />   
              <?php if ($errors['gebTag']): ?>
                <div class="field-error"><?= ($errors['gebTag']) ?></div>
              <?php endif; ?>           
              <label for="gebTag">Geburtstag</label>
            </div>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s6">
            <input id="kundNr" name="kundNr" type="text" class="validate" />
            <?php if ($errors['kundNr']): ?>
              <div class="field-error"><?= ($errors['kundNr']) ?></div>
            <?php endif; ?>
            <label for="kundNr">Kundinnen-Nr.</label>
          </div>        
        </div>
  
        <div class="row">
          <div class="input-field col s6">
            <input id="adresse" name="adresse" type="text" class="validate" />
            <?php if ($errors['adresse']): ?>
              <div class="field-error"><?= ($errors['adresse']) ?></div>
            <?php endif; ?>
            <label for="adresse">Adresse</label>          
          </div>
        </div>
  
        <div class="row">
          <div class="col s12">
            <div class="row">
              <div class="input-field col s3">
                <input id="plz" name="plz" type="text" class="validate" />
                <?php if ($errors['plz']): ?>
                  <div class="field-error"><?= ($errors['plz']) ?></div>
                <?php endif; ?>
                <label for="plz">PLZ</label>
              </div>
              <div class="input-field col s3">
                <input id="ort" name="ort"  type="text" class="validate" />
                <?php if ($errors['ort']): ?>
                  <div class="field-error"><?= ($errors['ort']) ?></div>
                <?php endif; ?>
                <label for="ort">Ort</label>
              </div>              
            </div>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s6">
            <input id="muttersprache" name="muttersprache" type="text" class="validate" />
            <?php if ($errors['muttersprache']): ?>
              <div class="field-error"><?= ($errors['muttersprache']) ?></div>
            <?php endif; ?>
            <label for="muttersprache">Muttersprache(n)</label>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s6">
            <textarea id="geschwister" name="geschwister" class="materialize-textarea"></textarea>
            <?php if ($errors['geschwister']): ?>
              <div class="field-error"><?= ($errors['geschwister']) ?></div>
            <?php endif; ?>
            <label for="geschwister">Geschwister</label>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s3">
            <input id="start" name="start" type="text" class="datepicker" />
            <?php if ($errors['start']): ?>
              <div class="field-error"><?= ($errors['start']) ?></div>
            <?php endif; ?>
            <label for="start">Start</label>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s3" id="gruppe">
            <select name="gruppe">
              <?php if ($errors['gruppe']): ?>
                <div class="field-error"><?= ($errors['gruppe']) ?></div>
              <?php endif; ?>
              <option value="1">Gruppe 1</option>
              <option value="2">Gruppe 2</option>
              <option value="3">Gruppe 3</option>
              <option value="4">Gruppe 4</option>
            </select>
            <label for="gruppe">Gruppe</label>
          </div>
        </div>
          
        <p>
          <label>
            <input type="checkbox" class="filled-in" name="fruehbetreuung" value="1"/>
            <?php if ($errors['fruehbetreuung']): ?>
              <div class="field-error"><?= ($errors['fruehbetreuung']) ?></div>
            <?php endif; ?>
            <span>Frühbetreuung</span>
          </label>
        </p>
  
        <p>
          <label>
            <input type="checkbox" class="filled-in" name="halbtags" value="1"/>
            <?php if ($errors['halbtags']): ?>
              <div class="field-error"><?= ($errors['halbtags']) ?></div>
            <?php endif; ?>
            <span>halbtags bis 14 Uhr</span>
          </label>
        </p>
  
        <p>
          <label>
            <input type="checkbox" class="filled-in" name="ganztags" value="1"/>
            <?php if ($errors['ganztags']): ?>
              <div class="field-error"><?= ($errors['ganztags']) ?></div>
            <?php endif; ?>
            <span>ganztags bis 16 Uhr</span>
          </label>
        </p>
  
        <div class="row">
          <div class="input-field col s6">
            <textarea id="allergien" name="allergien" class="materialize-textarea"></textarea>
            <?php if ($errors['allergien']): ?>
              <div class="field-error"><?= ($errors['allergien']) ?></div>
            <?php endif; ?>
            <label for="allergien">Allergie(n)</label>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s6">
            <textarea id="krankheiten" name="krankheiten" class="materialize-textarea"></textarea>
            <?php if ($errors['krankheiten']): ?>
              <div class="field-error"><?= ($errors['krankheiten']) ?></div>
            <?php endif; ?>
            <label for="krankheiten">Krankheite(n)</label>
          </div>
        </div>
  
        <p>
          <label>
            <input type="checkbox" class="filled-in" name="windeln" value="1"/>
            <?php if ($errors['windeln']): ?>
              <div class="field-error"><?= ($errors['windeln']) ?></div>
            <?php endif; ?>
            <span>Windeln</span>
          </label>
        </p>
  
        <p>
          <label>
            <input type="checkbox" class="filled-in" name="schnuller" value="1"/>
            <?php if ($errors['schnuller']): ?>
              <div class="field-error"><?= ($errors['schnuller']) ?></div>
            <?php endif; ?>
            <span>Schnuller</span>
          </label>
        </p>        
        
        <br><br><br>
      
        <button class="btn waves-effect waves-light" type="submit" name="speichern">Speichern</button>
      </form>
    </div>
  </div>
  
  
  <script>
    $(document).ready(function() {
      $(".datepicker").datepicker({format:"yyyy-mm-dd"});
    });
  
    $(document).ready(function() {
      $("select").formSelect();
    });
  </script> 
  